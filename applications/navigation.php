<?php
include '../include/config.php';
$links = [
	'home' => ['home', '/', '', 'knjaz__0003_home.png'],
	'sacuvaj' => ['sačuvaj', '', '_saveTree', 'knjaz__0000_Forma-1.png'],
	'preuzmi' => ['preuzmi', '', '_downloadTree', 'knjaz__0005_download.png'],
	'uputstvo' => ['uputstvo', '', '', 'knjaz__0006_question.png'],
	'podeli' => ['podeli', '', '_shareTree', 'knjaz__0004_share.png'],
	'napravi' => ['napravi stablo', SITE_DOMAIN . 'applications/create.php', '', 'knjaz__0002_tree.png'],
	'login' => ['login', 'logout.php', '', 'knjaz__0001_key.png']
];

?>
<nav class="navigation_bar">
	<?php
	foreach ($links as $key => $value) {
		if(in_array($key, $use_links)){
			if($value[2] === "_downloadTree")
				echo '<a id="download" download=\'porodicno_stablo.png\'>';
			echo '<button ' . ($value[2] === '_shareTree' ? 'data-clipboard-target="#clipboard"' : '') . ' class="menu_btn"' . (!empty($value[1]) ? ' onclick="javascript: window.location = \'' . $value[1] . '\'"' : '') . (!empty($value[2]) ? ' id="' . $value[2] . '"' : '') . '>' . $value[0] . '<img class="header-icon" src="../assets/images/'. $value[3] .'"></button>';
			if($value[2] === "_downloadTree")
				echo '</a>';
		}
	}
	?>
	<button class="menu_btn exit" onclick="javascript: window.location = 'logout.php';">&#10005;</button>
</nav>