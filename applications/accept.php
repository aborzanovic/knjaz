<?php
include '../include/config.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>JTree Genealogy Tree Maker Script - Readonly Family Tree</title>
    <link href="../assets/css/bootswatch/simplex/main.css" type="text/css" rel="stylesheet">
    <link href="../assets/css/tree.css" type="text/css" rel="stylesheet">
        <link href="../assets/css/evoke.css" type="text/css" rel="stylesheet">
</head>
<body>
    <script type="text/javascript">
        var checkIfAgreed = function(){
            if(!document.getElementsByName('selector')[0].checked) {
                alert("Morate se složiti sa uslovima korišćenja da biste nastavili.");
                return false;
            }
            return true;
        }
    </script>
    <div class="logo-container">
        <div class="logo large">
        </div>
        <div class="text-center accept_btn-container">
            <button class="accept_btn" onclick="javascript: if(checkIfAgreed()) window.location = 'create.php';">NAPRAVI SVOJE PORODIČNO STABLO</button>
            <br/>
            <input type="radio" id="s-option" name="selector">
            <label class="label" for="s-option">prihvatam <a href="http://knjaz.rs">uslove</a> korišćenja</label>
        </div>
        <p><?php echo $_SESSION['id']; ?></p>

        
    </div>

    

</body>
</html>
