<?php

if(!session_id()) {
    session_start();
}

header('Location: ./applications/accept.php');

require_once __DIR__ . '/app.php';


if(!isset($_SESSION['fb_access_token'])){

    $helper = $fb->getRedirectLoginHelper();

    try {
      $accessToken = $helper->getAccessToken();
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    if (! isset($accessToken)) {
      if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
      } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
      }
      exit;
    }

    // The OAuth 2.0 client handler helps us manage access tokens
    $oAuth2Client = $fb->getOAuth2Client();

    // Get the access token metadata from /debug_token
    $tokenMetadata = $oAuth2Client->debugToken($accessToken);

    // Validation (these will throw FacebookSDKException's when they fail)
    $tokenMetadata->validateAppId($appid); // Replace {app-id} with your app id
    // If you know the user ID this access token belongs to, you can validate it here
    //$tokenMetadata->validateUserId('123');
    $tokenMetadata->validateExpiration();

    if (! $accessToken->isLongLived()) {
      // Exchanges a short-lived access token for a long-lived one
      try {
        $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
      } catch (Facebook\Exceptions\FacebookSDKException $e) {
        echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
        exit;
      }

    }

    $_SESSION['fb_access_token'] = (string) $accessToken;
    $_SESSION['fb_user_id'] = $tokenMetadata->getProperty('user_id');
}

try {
  // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get('/me?fields=first_name,last_name,birthday,link,gender', $_SESSION['fb_access_token']);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

$user = $response->getGraphUser();

$params = [];

$params['id'] = $user['id'];
$params['name'] = $user['first_name'];
$params['last_name'] = $user['last_name'];
$params['profile_img'] = "//graph.facebook.com/$_SESSION[fb_user_id]/picture?type=large";
$params['sex'] = $user['gender'];
$params['birth_date'] = '';
$params['fb_url'] = $user['link'];

include './include/db.php';

if (isset($params['id'])) {
  $_SESSION['id'] = $params['id'];
  $_SESSION['name'] = $params['name'];
  $_SESSION['last_name'] = $params['last_name'];
  $_SESSION['profile_img'] = $params['profile_img'];
  $_SESSION['sex'] = $params['sex'];
  $_SESSION['birth_date'] = $params['birth_date'];
  $_SESSION['fb_url'] = $params['fb_url'];

  $db = new DB;
  $db->smartQuery(array(
    'sql' => 'insert ignore into users (familyid,id) values (0,?)',
    'par' => [$params['id']],
    'ret' => 'res'
  ));
}
else die('no id set');