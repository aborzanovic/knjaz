var doFBLogin = function () {
    /*
    TODO
    dohvatiti korisnicke parametre sa fb i zameniti hardcode-ovane
    */
    createUserIfNotExists(
        '123456789',
        'Knjaz',
        'Milos',
        'http://www.vincegolangco.com/wp-content/uploads/2010/12/batman-for-facebook.jpg',
        'Muški',
        '31/apr/93',
        'https://www.facebook.com/zuck'
    );
}

var createUserIfNotExists = function (id, name, last_name, profile_img, sex, birth_date, fb_url) {
    $.ajax({
        type: 'POST',
        url: '/applications/save_user.php',
        data: {
            id: id,
            name: name,
            last_name: last_name,
            profile_img: profile_img,
            sex: sex,
            birth_date: birth_date,
            fb_url: fb_url
        },
        success: function (msg) {
            window.location = 'applications/accept.php';
        },
        error: function (msg) {
            alert("Došlo je do greške sa konekcijom. Pokušajte ponovo kasnije.")
        }
    });
}

$('#tcontainer').on({
	click: function (e) {
		$('#clipboard')[0].value = getShareUrl();
        var fid = parseInt(familyID);
        if(isNaN(fid) || fid <= 0)
            $.notify("morate prvo sačuvati stablo!", "warn");
        else
            $.notify("link za deljenje kopiran!", "success");
	}
}, '#_shareTree');

$('#tcontainer').on({
    click: function (e) {
        html2canvas(document.body, {
          onrendered: function(canvas) {
            var download = document.getElementById("download");
            var image = canvas.toDataURL("image/png")
                        .replace("image/png", "image/octet-stream");
            download.setAttribute("href", image);
          }
        });
    }
}, '#_downloadTree');

var getShareUrl = function () {
	return dn + '/applications/readonly.php?id=' + familyID;
}

$(function() {
	
    $('#scroll-mode').change(function() {
      if($(this).prop('checked'))
        $('#vcart').overscroll();
      else
        $('#vcart').removeOverscroll();
    });

    new Clipboard('#_shareTree');
});