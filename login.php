<?php

if(!session_id()) {
    session_start();
}
require_once __DIR__ . '/app.php';

//if(!isset($_SESSION['fb_access_token'])){

    $helper = $fb->getRedirectLoginHelper();

    $permissions = ['user_posts']; // Optional permissions
    $loginUrl = $helper->getLoginUrl(URL . 'fb-callback.php', $permissions);

    echo '<a class="fb_btn" href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
    return;
//} else {
    //header("Location: fb-callback.php");

//}