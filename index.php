<?php
include '../include/config.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Knjaz Miloš</title>
    <link href="assets/css/bootswatch/simplex/main.css" type="text/css" rel="stylesheet">
    <link href="assets/css/tree.css" type="text/css" rel="stylesheet">
        <link href="assets/css/evoke.css" type="text/css" rel="stylesheet">
</head>
<body>
    <div class="logo-container">
        <div class="logo large">
        </div>
        <div class="text-center fb_btn-container">
            <?php 
            	include('login.php');
            ?>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="assets/js/evoke.js"></script>

</body>
</html>
